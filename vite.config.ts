import { defineConfig } from 'vitest/config'

export default defineConfig({
  base: './',
  build: {
    lib: {
      entry: 'src/index.ts',
      name: 'Spice'
    }
  },
  test: {
  },
})
