# Spice Typescript

# List of "new" Javascript tecnologies:
- BigInt
- Streams API (`ReadableStream` and `ReadableStreamBYOBReader`)
- SubtleCrypto API
- Web Workers
- `AudioWorklet`
- OffscreenCanvas
- WebGPU
