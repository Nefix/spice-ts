import { QuicImageType, QuicImageParams } from "../../gen/spice";

export class Encoder {
    static quicImageParams(type: QuicImageType): QuicImageParams {
        switch (type) {
            case QuicImageType.QuicImageTypeGray:
                return new QuicImageParams(1, 8)

            case QuicImageType.QuicImageTypeRgb16:
                return new QuicImageParams(3, 5)

            case QuicImageType.QuicImageTypeRgb24:
                return new QuicImageParams(3, 8)

            case QuicImageType.QuicImageTypeRgb32:
                return new QuicImageParams(3, 8)

            case QuicImageType.QuicImageTypeRgba:
                return new QuicImageParams(4, 8)

            default:
                throw ("Bad QUIC image type: " + type)
        }
    }

    static decode(type: QuicImageType, buf: ArrayBuffer) {
        switch (type) {
            case QuicImageType.QuicImageTypeRgb32:
                this.uncompressRGB()
                break

            default:
                throw ("Unknown QUIC image type for decoding: " + type)
        }
    }

    static uncombressRGB()
}