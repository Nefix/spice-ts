import { Lz } from "../../gen/spice"
import { LzImageType } from "../../gen/spice"

class lz {
    // assigns the pixel to the place pointed by out and increases dstOffset. Used in RLE. Need special handling because in alpha we copy only the pad byte.
    static copyPixel(dstBuf: Uint8Array, dstOffset: number, ref: number): number {
        dstBuf[dstOffset++] = dstBuf[ref]
        return dstOffset
    }

    // copies the pixel pointed by ref to the pixel pointed by out. Increases ref and dstOffset.
    static copyRefPixel(dstBuf: Uint8Array, dstOffset: number, ref: number): [number, number] {
        dstBuf[dstOffset++] = dstBuf[ref++]
        return [dstOffset, ref]
    }

    // copies pixel from the compressed buffer to the decompressed buffer. Increases dstOffset.
    static copyCompressedPixel(srcBuf: Uint8Array, srcOffset: number, dstBuf: Uint8Array, dstOffset: number): [number, number] {
        throw "Not implemented"
    }

    static decompress(srcBuf: Uint8Array, length: number, dstBuf: Uint8Array) {
        let srcOffset = 0
        let dstOffset = 0

        while (true) {
            let ctrl = srcBuf[srcOffset++]

            if (srcOffset < 10) {
                console.log(ctrl)
                console.log(srcOffset)
                console.log("||")
            }

            // Check if it's a match operation (long or short)
            // This is because a literal run always has the 3 least significant
            // bits as '0' (0b000xxxxx)
            if (ctrl >= 32) {
                // We take the 3 most significative bits for the match length
                // If it's a short match, we'll live it as is
                // If it's a long match, we'll need to complement it with the following
                // instruction byte (opcode[1])
                // TODO: We remove 1 because ???
                let matchLen = (ctrl >> 5) - 1

                // We take the 
                let ofs = (ctrl & 31) << 8;

                // 
                let ref = dstOffset - ofs - 1

                // If it's a long run*, add the second instruction byte (opcode[1])
                // for the length
                // *we know this because long runs always have 3 least significant bits
                // as '1' (0b111xxxxx) 
                let code: number = 0
                if (matchLen == 7 - 1) {
                    // Here, the SPICE LZ77 implementation differs from the
                    // FastLZ one. In the SPICE implementation, a long run can
                    // have more than 3 bytes. The "extra" bytes are in the middle
                    do {
                        code = srcBuf[srcOffset++]
                        matchLen += code
                    } while (code === 255)
                }
                code = srcBuf[srcOffset++];
                ofs += code;

                // if (code === 255) {
                //     if ()
                // }

                // Complement the rest of the reference.
                // This works in both short and long matches because if it's
                // a long match, we've just jumped the length byte (opcode[1]) and
                // we're already at opcode[2]
                ref -= srcBuf[srcOffset++];

                // We recover the 1 that we removed at definition, and add +2, since
                // if it's a short match
                //     1 -> 3 (3 - 1 = 2)
                //     2 -> 4
                //     ...
                // if it's a long match
                //     0 -> 9 (we have 7 at opcode[0], 9 - 7 = 2)
                //     1 -> 10
                //     ...
                // TODO: Here, both the C and JS implementation change the length
                // bias depending on the image type, but the FastLZ doesn't
                matchLen += 3

                // special handling for the last byte, because in Alpha
                // we only copy the pad byte
                if (ref == (dstOffset - 1)) {
                    // Since it's the last byte, we're going to copy
                    // it the times is needed
                    for (; matchLen > 0; matchLen--) {
                        dstOffset = this.copyPixel(dstBuf, dstOffset, ref)
                    }

                } else {
                    for (; matchLen > 0; matchLen--) {
                        [dstOffset, ref] = this.copyRefPixel(dstBuf, ref, dstOffset)
                    }
                }

                // TODO: Ensure this is already being handled by the copyPixel / copyRefPixel functions
                // dstOffset += matchLen

                // Literal run
            } else {
                // TODO: IMAGE ENCODING STUFF
                // We add +1, since 0 is 1 byte, 1 is 2 bytes, etc
                for (let run = ctrl + 1; run > 0; run--) {
                    [srcOffset, dstOffset] = this.copyCompressedPixel(srcBuf, srcOffset, dstBuf, dstOffset)
                }
            }

            if (srcOffset >= length) {
                break
            }
        }

        // TODO: Return
        // return dstOffset - 
    }
}

class lzRGB32 extends lz {
    // RGB32 consists of
    // uint8 r
    // uint8 g
    // uint8 b
    // uint8 pad
    // (hence, the 32 bits xD)
    static copyCompressedPixel(srcBuf: Uint8Array, srcOffset: number, dstBuf: Uint8Array, dstOffset: number): [number, number] {
        dstBuf[dstOffset++] = srcBuf[srcOffset + 2] // b
        dstBuf[dstOffset++] = srcBuf[srcOffset + 1] // g
        dstBuf[dstOffset++] = srcBuf[srcOffset + 0] // r
        dstBuf[dstOffset++] = 0 // pad is always 0
        srcOffset += 3

        return [srcOffset, dstOffset]
    }
}

export class Encoder {
    static decode(image: Lz, buf: ArrayBuffer): ArrayBuffer {
        switch (image.type) {
            case LzImageType.LzImageTypeRgb32:
                const dst = new ArrayBuffer(image.width * image.height * 4) // Each pixel is 32 bits (4 bytes)
                lzRGB32.decompress(new Uint8Array(buf), buf.byteLength, new Uint8Array(dst))
                return dst
            default:
                throw "Unknown LZ image type for decoding: " + LzImageType[image.type]
        }
    }
}