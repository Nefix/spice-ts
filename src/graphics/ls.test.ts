import { expect, test } from 'vitest'
import { Lz, LzImageType, LzMagic, MsgDisplayDrawCopy } from '../../gen/spice'
import { lz_rgb32_decompress } from './lz.js'
import { Encoder } from './lz'


async function readBin(path: string): Promise<Buffer> {
    const f = await Bun.file(path).text() as String
    const buf: number[] = []
    f.split(",").forEach((c) => {
        buf.push(parseInt(c, 10))
    })

    return Buffer.from(buf)
}

test('lz_rgb32 should work correctly', async () => {
    const expectedMsg = await readBin("src/graphics/bin/msg_display_draw_copy_lz.bin")

    const unmarshalBuf = new ArrayBuffer(expectedMsg.byteLength)
    const view = new DataView(unmarshalBuf)
    let offset = 0
    expectedMsg.forEach((i) => {
        view.setUint8(offset, i)
        offset += 1
    })

    const msg = MsgDisplayDrawCopy.unmarshal(unmarshalBuf)
    const lz = Lz.unmarshal(msg.data.srcBitmap?.lzRgb?.data!)

    // Ensure the lz is valid and it's a rgb32 image
    expect(lz.magic == LzMagic).toBe(true)
    expect(lz.type == LzImageType.LzImageTypeRgb32).toBe(true)

    const decompressed = new ArrayBuffer(lz.height * lz.width * 4)
    lz_rgb32_decompress(new Uint8Array(msg.data.srcBitmap?.lzRgb?.data!), Lz.messageSize - 1, new Uint8Array(decompressed), LzImageType.LzImageTypeRgb32, true)

    console.log('~~~~')

    const ourDecompressed = Encoder.decode(lz, msg.data.srcBitmap?.lzRgb?.data!.slice(Lz.messageSize - 1)!)

    console.log(decompressed)
    console.log(ourDecompressed)
})