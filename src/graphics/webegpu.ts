import { Quic } from '../../gen/spice'
import quic from './quic.wgsl?raw'

export class WebGPU {
    device: GPUDevice

    constructor(device: GPUDevice) {
        this.device = device

        // QUIC
        device.createShaderModule({
            label: "QUIC decoding module",
            code: quic,
        })

        // LZ

        // ...
    }

    decodeQUIC(msg: Quic) {
        const input = new ArrayBuffer(3);

        const workBuffer = this.device.createBuffer({
            label: "QUIC decoding buffer",
            size: input.byteLength,
            usage: GPUBufferUsage.STORAGE | GPUBufferUsage.COPY_SRC | GPUBufferUsage.COPY_DST
        })
        this.device.queue.writeBuffer(workBuffer, 0, input)

        const resultBuffer = this.device.createBuffer({
            label: 'result buffer',
            size: input.byteLength,
            usage: GPUBufferUsage.MAP_READ | GPUBufferUsage.COPY_DST
        });
    }
}