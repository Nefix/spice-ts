import { MsgPlaybackStart, SpiceChannel } from "../gen/spice";

export enum WorkerMsg {
    NewChannel,
    MainResize,
    MainAgentConnected,
    DisplayCanvas,
    InputsKeyDown,
    InputsKeyUp,
    InputsMouseMove,
    InputsMouseDown,
    InputsMouseUp,
    PlaybackStart,
    PlaybackData,
}

export interface Msg {
    type: WorkerMsg
    data: NewChannel | MainResize | DisplayCanvas | InputsKeyDown | InputsKeyUp | InputsMouseMove | InputsMouseDown | InputsMouseUp | PlaybackStart | PlaybackData
}

export interface NewChannel {
    type: SpiceChannel,
    url?: string,
    pwd?: string,
    connId?: number,
    chanId?: number,
}

export interface MainResize {
    height: number,
    width: number,
}

export interface DisplayCanvas {
    canvas: OffscreenCanvas
}

export interface InputsKeyDown {
    event: KeyboardEvent
}

export interface InputsKeyUp {
    event: KeyboardEvent
}

export interface InputsMouseMove {
    event: MouseEvent
}

export interface InputsMouseDown {
    event: MouseEvent
}

export interface InputsMouseUp {
    event: MouseEvent
}

export interface PlaybackStart {
    msg: MsgPlaybackStart
}

export interface PlaybackData {
    buf: ArrayBuffer
}
