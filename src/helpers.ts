export class Buffer {
    buf: ReadableStream
    reader: ReadableStreamBYOBReader

    constructor(conn: WebSocket) {
        conn.binaryType = 'arraybuffer'

        this.buf = new ReadableStream({
            type: "bytes",
            start(controller) {
                conn.onmessage = (ev) => {
                    controller.enqueue(new Uint8Array(ev.data))
                }
            }
        })
        this.reader = this.buf.getReader({ mode: "byob" })
    }

    async read(length: number): Promise<ArrayBuffer> {
        let offset = 0;
        let buf = new ArrayBuffer(length);

        while (offset < length) {
            const { done, value } = await this.reader.read(new Uint8Array(buf, offset, length - offset))
            if (done) {
                throw "Stream finished before reading all the required bytes!"
            }

            buf = value.buffer
            offset += value.byteLength
        }

        console.debug(">>> ", buf)
        return buf
    }
}

export const listToArrayBuffer = (list: Array<any>): ArrayBuffer => {
    const buf = new ArrayBuffer(list.length)
    const bufView = new Uint8Array(buf)

    list.forEach((c, i) => {
        bufView[i] = c
    })

    return buf
}

export const strToArrayBuffer = (str: String): ArrayBuffer => {
    const buf = new ArrayBuffer(str.length)
    const bufView = new Uint8Array(buf)

    for (let i = 0; i < str.length; i++) {
        bufView[i] = str.charCodeAt(i)
    }

    return buf
}

// export const classToObject = (c: Object): Object => {
//     const obj: { [k: string]: any } = {}
//     const props = Object.getOwnPropertyDescriptors(Object.getPrototypeOf(c))
//     for (const prop in props) {
//         if (prop === "fromElement" || prop === "toElement") {
//             continue
//         }

//         const descriptor = props[prop]
//         const getter = descriptor.get

//         if (getter) {
//             obj[prop] = getter.call(c)
//         } else {
//             if (typeof descriptor.value !== "function") {
//                 obj[prop] = descriptor.value
//             }
//         }
//     }

//     for (const k in c) {
//         if (c.hasOwnProperty(k)) {
//             obj[k] = c[k]
//         }
//     }

//     return obj
// }