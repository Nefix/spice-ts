import { KeyboardModifierFlags, MouseButtonMask, MsgInputs, MsgInputsInit, MsgcInputs, MsgcInputsKeyDown, MsgcInputsKeyUp, MsgcInputsMouseMotion, MsgcInputsMousePosition, MsgcInputsMousePress, MsgcInputsMouseRelease, SpiceChannel, SpiceInputMotionAckBunch, SpiceMiniDataHeader } from "../../gen/spice";
import { Channel } from "./channel";

export class InputsChannel extends Channel {
    keyboardModifiers = KeyboardModifierFlags.Empty
    mouseButtonsState = MouseButtonMask.Empty
    mouseMotionWaitingForAck = 0

    constructor(url: string, pwd: string, connId?: number, chanId?: number) {
        super(url, SpiceChannel.SpiceChannelInputs, pwd, connId, chanId)
    }

    async processChannelMsg(h: SpiceMiniDataHeader) {
        switch (h.type) {
            case MsgInputs.MsgInputsInit:
                await this.msgInputsInit(h)
                break

            case MsgInputs.MsgInputsMouseMotionAck:
                await this.msgInputsMouseMotionAck(h)
                break

            default:
                throw ("Unsupported message type: " + MsgInputs[h.type])
        }
    };

    async msgInputsInit(h: SpiceMiniDataHeader) {
        let buf = await this.buf.read(h.size)
        const msg = MsgInputsInit.unmarshal(buf)
        console.debug(">>> ", msg)

        this.keyboardModifiers = msg.keyboardModifiers
    }

    async msgInputsMouseMotionAck(_: SpiceMiniDataHeader) {
        this.mouseMotionWaitingForAck -= SpiceInputMotionAckBunch
    }

    keyDown(_: KeyboardEvent) {
        const msg = new MsgcInputsKeyDown(23)
        this.sendMsg(MsgcInputs.MsgcInputsKeyDown, msg.marshal())
    }

    keyUp(_: KeyboardEvent) {
        const msg = new MsgcInputsKeyUp(23 | 0x80)
        this.sendMsg(MsgcInputs.MsgcInputsKeyUp, msg.marshal())
    }

    mouseMove(event: MouseEvent) {
        // TODO: We're assuming that the mode is client

        // If the ack queue is too long, discard the events
        if (this.mouseMotionWaitingForAck < (SpiceInputMotionAckBunch * 2)) {
            // TODO: Display = 0. Multidisplay?
            const msg = new MsgcInputsMousePosition(event.x, event.y, this.mouseButtonsState, 0)
            this.sendMsg(MsgcInputs.MsgcInputsMousePosition, msg.marshal())
            this.mouseMotionWaitingForAck++
        }
    }

    mouseDown(event: MouseEvent) {
        this.mouseButtonsState = 1 << event.button
        const msg = new MsgcInputsMousePress(event.button + 1, this.mouseButtonsState)
        this.sendMsg(MsgcInputs.MsgcInputsMousePress, msg.marshal())
    }

    mouseUp(event: MouseEvent) {
        this.mouseButtonsState = MouseButtonMask.Empty
        const msg = new MsgcInputsMouseRelease(event.button + 1, this.mouseButtonsState)
        this.sendMsg(MsgcInputs.MsgcInputsMouseRelease, msg.marshal())
    }
}