import { MouseMode, MsgMain, MsgMainChannelsList, MsgMainInit, MsgMainMouseMode, MsgcMain, MsgcMainAttachChannels, MsgcMainMouseModeRequest, SpiceChannel, SpiceMiniDataHeader } from "../../gen/spice";
import { Agent } from "./agent";
import { Channel } from "./channel";

export class MainChannel extends Channel {
    newChannel: (type: SpiceChannel, connId: number, chanId: number) => void
    onAgentConnected: () => void

    mouseMode?: MouseMode
    agent?: Agent

    constructor(url: string, pwd: string, newChannel: (type: SpiceChannel, connId: number, chanId: number) => void, onAgentConnected: () => void, connId?: number, chanId?: number,) {
        super(url, SpiceChannel.SpiceChannelMain, pwd, connId, chanId)

        this.newChannel = newChannel
        this.onAgentConnected = onAgentConnected
    }

    async processChannelMsg(h: SpiceMiniDataHeader) {
        switch (h.type) {
            case MsgMain.MsgMainInit:
                await this.msgMainInit(h)
                break
            case MsgMain.MsgMainChannelsList:
                await this.msgMainChannelsList(h)
                break

            case MsgMain.MsgMainMouseMode:
                await this.msgMainMouseMode(h)
                break

            // VDAgent
            case MsgMain.MsgMainAgentConnected:
                await this.buf.read(h.size)
                this.agent = new Agent(this)
                this.onAgentConnected()
                break

            case MsgMain.MsgMainAgentDisconnected:
            case MsgMain.MsgMainAgentData:
            case MsgMain.MsgMainAgentToken:
            case MsgMain.MsgMainAgentConnectedTokens:
                if (this.agent !== undefined) {
                    await this.agent.processMsg(h)

                } else {
                    console.error('Recieved agent message, but the agent is not connected: ', h)
                }

                break

            default:
                throw ("Unsupported message type: " + MsgMain[h.type])
        }
    };

    async msgMainInit(h: SpiceMiniDataHeader) {
        let buf = await this.buf.read(h.size)
        const msg = MsgMainInit.unmarshal(buf)
        console.debug(">>> ", msg)

        console.info("New channel:")
        console.info("-----------")
        console.info("sessionId: ", msg.sessionId)
        console.info("displayChannelsHint: ", msg.displayChannelsHint)
        console.info("supportedMouseModes: ", msg.supportedMouseModes)
        console.info("currentMouseMode: ", msg.currentMouseMode)
        console.info("agentConnected: ", msg.agentConnected)
        console.info("agentTokens: ", msg.agentTokens)
        console.info("multiMediaTime: ", msg.multiMediaTime)
        console.info("ramHint: ", msg.ramHint)

        this.connId = msg.sessionId
        // TODO: Agent tokens
        // TODO: Multimedia time (server's and ours)

        this.setMouseMode(msg.currentMouseMode, msg.supportedMouseModes)

        if (msg.agentConnected === 1) {
            this.agent = new Agent(this)
            this.onAgentConnected()
        }

        const att = new MsgcMainAttachChannels()
        this.sendMsg(MsgcMain.MsgcMainAttachChannels, att.marshal())
    }

    async msgMainChannelsList(h: SpiceMiniDataHeader) {
        let buf = await this.buf.read(h.size)
        const msg = MsgMainChannelsList.unmarshal(buf)
        console.debug(">>> ", msg)

        // msg.channels.forEach((c) => {
        //     if (
        //         c.type === SpiceChannel.SpiceChannelPlayback ||
        //         c.type === SpiceChannel.SpiceChannelInputs ||
        //         c.type === SpiceChannel.SpiceChannelDisplay
        //     ) {
        //         return
        //     }
        //     this.newChannel(c.type, this.connId, c.id)
        // })
    }

    setMouseMode(current: MouseMode, supported: MouseMode) {
        this.mouseMode = current

        if (
            current != MouseMode.MouseModeClient
            && (supported & MouseMode.MouseModeClient)
        ) {
            const msg = new MsgcMainMouseModeRequest(MouseMode.MouseModeClient)
            this.sendMsg(MsgcMain.MsgcMainMouseModeRequest, msg.marshal())
        }

        // TODO: Set the inputs!
    }

    async msgMainMouseMode(h: SpiceMiniDataHeader) {
        let buf = await this.buf.read(h.size)
        const msg = MsgMainMouseMode.unmarshal(buf)

        this.setMouseMode(msg.currentMode, msg.supportedModes)
    }

    resize(height: number, width: number) {
        if (this.agent) {
            this.agent.resize(height, width)
        } else {
            console.error('Attempted to resize, but agent is not connected')
        }
    }
}