import { MsgCursor, MsgCursorInit, SpiceChannel, SpiceMiniDataHeader } from "../../gen/spice";
import { Channel } from "./channel";

export class CursorChannel extends Channel {
    constructor(url: string, pwd: string, connId?: number, chanId?: number) {
        super(url, SpiceChannel.SpiceChannelCursor, pwd, connId, chanId)
    }

    async processChannelMsg(h: SpiceMiniDataHeader) {
        switch (h.type) {
            case MsgCursor.MsgCursorInit:
                await this.msgCursorInit(h)
                break

            default:
                throw ("Unsupported message type: " + MsgCursor[h.type])
        }
    }

    async msgCursorInit(h: SpiceMiniDataHeader) {
        const buf = await this.buf.read(h.size)
        const msg = MsgCursorInit.unmarshal(buf)
        console.debug(">>> ", msg)
    }
}