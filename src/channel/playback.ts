import { MsgPlayback, MsgPlaybackMode, MsgPlaybackStart, SpiceChannel, SpiceMiniDataHeader } from "../../gen/spice";
import { Channel } from "./channel";

export type playbackStart = (msg: MsgPlaybackStart) => void
export type playbackData = (buf: ArrayBuffer) => void

export class PlaybackChannel extends Channel {
    playbackStart: playbackStart
    playbackData: playbackData

    channels?: number
    frequency?: number

    constructor(url: string, pwd: string, playbackStart: playbackStart, playbackData: playbackData, connId?: number, chanId?: number) {
        super(url, SpiceChannel.SpiceChannelPlayback, pwd, connId, chanId)

        this.playbackStart = playbackStart
        this.playbackData = playbackData
    }

    async processChannelMsg(h: SpiceMiniDataHeader) {
        switch (h.type) {
            case MsgPlayback.MsgPlaybackMode:
                await this.msgPlaybackMode(h)
                break

            case MsgPlayback.MsgPlaybackStart:
                await this.msgPlaybackStart(h)
                break

            case MsgPlayback.MsgPlaybackData:
                await this.msgPlaybackData(h)
                break

            default:
                throw ("Unsupported message type: " + MsgPlayback[h.type])
        }
    }

    async msgPlaybackMode(h: SpiceMiniDataHeader) {
        let buf = await this.buf.read(h.size)
        const msg = MsgPlaybackMode.unmarshal(buf)
        console.debug(">>> ", msg)
        // TODO: This!:)
    }

    async msgPlaybackStart(h: SpiceMiniDataHeader) {
        let buf = await this.buf.read(h.size)
        const msg = MsgPlaybackStart.unmarshal(buf)
        console.debug(">>> ", msg)

        this.channels = msg.channels
        this.frequency = msg.frequency

        this.playbackStart(msg)
    }

    async msgPlaybackData(h: SpiceMiniDataHeader) {
        let buf = await this.buf.read(h.size)
        this.playbackData(buf)
    }
}
