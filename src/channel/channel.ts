import { LinkErr, Msg, MsgNotify, MsgPing, MsgSetAck, Msgc, MsgcAck, MsgcAckSync, MsgcPong, NotifySeverity, SpiceChannel, SpiceCommonCap, SpiceLinkAuthMechanism, SpiceLinkHeader, SpiceLinkMess, SpiceLinkReply, SpiceMagic, SpiceMiniDataHeader, SpiceVersionMajor, SpiceVersionMinor } from '../../gen/spice'
import { Buffer, listToArrayBuffer, strToArrayBuffer } from '../helpers'

export class Channel {
    conn: WebSocket
    buf: Buffer

    state: ChannelState

    pwd: string
    connId: number = 0
    type: SpiceChannel
    chanId: number = 0

    ackWindow?: number
    ackPending = 0

    constructor(url: string, type: SpiceChannel, pwd: string, connId?: number, chanId?: number) {
        this.conn = new WebSocket(url)
        this.buf = new Buffer(this.conn)

        this.state = ChannelState.Stopped
        this.type = type

        this.pwd = pwd

        if (connId !== undefined) {
            this.connId = connId
        }
        if (chanId !== undefined) {
            this.chanId = chanId
        }

        this.conn.onopen = (_) => {
            this.worker()
        }
    }

    async worker() {
        while (true) {
            switch (this.state) {
                case ChannelState.Stopped:
                    this.start()
                    break
                case ChannelState.Started:
                    await this.link()
                    break
                case ChannelState.Linked:
                    await this.ticket()
                    break
                case ChannelState.Ticketed:
                    this.ready()
                    break
                case ChannelState.Ready:
                    if (this.ackWindow) {
                        this.ackPending++

                        if (this.ackWindow == this.ackPending) {
                            const msg = new MsgcAck()
                            this.sendMsg(Msgc.MsgcAck, msg.marshal())
                        }
                    }

                    let buf = await this.buf.read(SpiceMiniDataHeader.messageSize)
                    const h = SpiceMiniDataHeader.unmarshal(buf)
                    console.debug(">>> ", h)

                    if (h.type <= Msg.MsgBaseLast) {
                        await this.processSpiceMsg(h)
                    } else {
                        await this.processChannelMsg(h)
                    }
                    break
                case ChannelState.Err:
                    throw ("Channel state error!")
            }
        }
    }


    async processChannelMsg(_: SpiceMiniDataHeader) {
        throw ("Channel doens't implement channel message handing: " + SpiceChannel[this.type])
    }

    async processSpiceMsg(h: SpiceMiniDataHeader) {
        switch (h.type) {
            case Msg.MsgSetAck:
                await this.msgSetAck(h)
                break

            case Msg.MsgPing:
                await this.msgPing(h)
                break
            // case Msg.MsgWaitForChannels:
            //     await this.waitForChannels(h)
            //     break

            case Msg.MsgNotify:
                await this.msgNotify(h)
                break

            default:
                throw ("Unsupported spice message: " + Msg[h.type])
        }
    }

    start() {
        console.debug("channel start")

        const h = new SpiceLinkHeader(
            SpiceMagic,
            SpiceVersionMajor,
            SpiceVersionMinor,
            SpiceLinkMess.messageSize + 4, // SpiceMiniHeader!
        )
        const msg = new SpiceLinkMess(
            this.connId,
            this.type,
            this.chanId,
            // SpiceMiniHeader!
            1, // TODO: This!
            0, // TODO: This!
            SpiceLinkMess.messageSize,
        )

        const hBuf = h.marshal()
        const msgBuf = msg.marshal()

        // SpiceMiniHeader!
        const cmmnCapsBuf = new ArrayBuffer(4)
        const cmmnCapsBufView = new DataView(cmmnCapsBuf)
        cmmnCapsBufView.setUint32(0, 1 << SpiceCommonCap.SpiceCommonCapMiniHeader, true)

        console.debug("<<< ", h)
        console.debug("    ", hBuf)
        console.debug("<<< ", msg)
        console.debug("    ", msgBuf)
        console.debug("<<< ", cmmnCapsBuf)

        this.conn.send(hBuf)
        this.conn.send(msgBuf)
        this.conn.send(cmmnCapsBuf)

        this.state = ChannelState.Started
    }

    async link() {
        console.debug("channel link")

        let buf = await this.buf.read(SpiceLinkHeader.messageSize)
        const h = SpiceLinkHeader.unmarshal(buf)
        console.debug(">>> ", h)

        if (h.magic != SpiceMagic) {
            console.error('unknown spice magic: ', h.magic)
            return
        }

        // TODO: Check versions
        buf = await this.buf.read(SpiceLinkReply.messageSize)
        const rsp = SpiceLinkReply.unmarshal(buf)
        console.debug(">>> ", rsp)

        // TODO: Check server capabilities (mini header)
        let cmmnCaps: SpiceCommonCap[] = []
        for (let i = 0; i < rsp.numCommonCaps; i++) {
            buf = await this.buf.read(4) // uint32
            const view = new DataView(buf)
            cmmnCaps.push(view.getUint32(0, true))
        }
        console.debug('>>> Server common caps: ', cmmnCaps)

        let chanCaps: number[] = []
        for (let i = 0; i < rsp.numChannelCaps; i++) {
            buf = await this.buf.read(4) // uint32
            const view = new DataView(buf)
            chanCaps.push(view.getUint32(0, true))
        }
        console.debug('>>> Server channel caps: ', chanCaps)


        if (rsp.error != LinkErr.LinkErrOk) {
            console.error('channel link error: ', LinkErr[rsp.error])
            throw ('channel link error: ' + LinkErr[rsp.error])
        }

        const keyBuf = listToArrayBuffer(rsp.pubKey)
        const pwdBuf = strToArrayBuffer(this.pwd)

        const pub = await crypto.subtle.importKey("spki", keyBuf, { name: "RSA-OAEP", hash: "SHA-1" }, true, ["encrypt"])
        const crypted = await crypto.subtle.encrypt({ name: 'RSA-OAEP' }, pub, pwdBuf)

        console.debug("<<< ", crypted)
        this.conn.send(crypted)

        this.state = ChannelState.Linked
    }

    async ticket() {
        console.debug("channel ticket")

        let buf = await this.buf.read(SpiceLinkAuthMechanism.messageSize)
        const rsp = SpiceLinkAuthMechanism.unmarshal(buf)
        console.debug(">>> ", rsp)

        if (rsp.authMechanism !== LinkErr.LinkErrOk) {
            console.error('channel ticket error: ', LinkErr[rsp.authMechanism])
            throw ('channel ticket error: ' + LinkErr[rsp.authMechanism])
        }

        this.state = ChannelState.Ticketed
    }

    ready() {
        console.debug("channel ready")
        this.onReady()

        this.state = ChannelState.Ready
    }

    onReady() { }

    sendMsg(type: number, msg: ArrayBuffer) {
        const h = new SpiceMiniDataHeader(type, msg.byteLength)
        console.debug("<<< ", h)
        console.debug("<<< ", msg)

        this.conn.send(h.marshal())
        this.conn.send(msg)
    }

    async msgSetAck(h: SpiceMiniDataHeader) {
        let buf = await this.buf.read(h.size)
        const msg = MsgSetAck.unmarshal(buf)

        this.ackWindow = msg.window

        const rsp = new MsgcAckSync(msg.generation)
        this.sendMsg(Msgc.MsgcAckSync, rsp.marshal())
    }

    async msgPing(h: SpiceMiniDataHeader) {
        let buf = await this.buf.read(h.size)
        const msg = MsgPing.unmarshal(buf)
        console.debug(">>> ", msg)

        const rsp = new MsgcPong(msg.id, msg.timestamp)
        this.sendMsg(Msgc.MsgcPong, rsp.marshal())
    }

    async msgWaitForChannels(_: SpiceMiniDataHeader) {
        // await this.buf.read(h.size)

        // let buf = await this.buf.read(h.size)
        // console.debug(h.size)
        // console.debug(buf.byteLength)
        // const msg = MsgWaitForChannels.unmarshal(buf, h.size)

        // console.debug("CHAN GOOD!: )")
        // console.debug(">>> ", msg)
    }

    async msgNotify(h: SpiceMiniDataHeader) {
        let buf = await this.buf.read(h.size)
        const msg = MsgNotify.unmarshal(buf)
        console.debug(">>> ", msg)

        const msgStr = new TextDecoder("utf-8").decode(Uint8Array.from(msg.message))
        switch (msg.severity) {
            case NotifySeverity.NotifySeverityError:
                console.error("Spice notification: ", msgStr)
                break
            case NotifySeverity.NotifySeverityWarn:
                console.warn("Spice notification: ", msgStr)
                break

            default:
                console.info("Spice notification: ", msgStr)
        }
    }
}

enum ChannelState {
    Stopped = 0,
    Started = 1,
    Linked = 2,
    Ticketed = 3,
    Ready = 4,
    Err = 5,
}
