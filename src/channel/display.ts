import { ImageType, Lz, MsgDisplay, MsgDisplayDrawCopy, MsgDisplayInvalAllPalettes, MsgDisplayMark, MsgDisplaySurfaceCreate, MsgcDisplay, MsgcDisplayInit, Quic, SpiceChannel, SpiceMiniDataHeader } from "../../gen/spice";
import { Channel } from "./channel";

export class DisplayChannel extends Channel {
    canvas: OffscreenCanvas[] = []
    context: GPUCanvasContext[] = []

    adapter: GPUAdapter | null = null
    device: GPUDevice | null = null
    presentationFormat: GPUTextureFormat

    cacheSize = 10 * 1024 * 1024

    constructor(url: string, pwd: string, connId?: number, chanId?: number) {
        super(url, SpiceChannel.SpiceChannelDisplay, pwd, connId, chanId)

        this.presentationFormat = navigator.gpu.getPreferredCanvasFormat()
    }

    async processChannelMsg(h: SpiceMiniDataHeader) {
        switch (h.type) {
            case MsgDisplay.MsgDisplayInvalAllPalettes:
                await this.msgDisplayInvalAllPalettes(h)
                break

            case MsgDisplay.MsgDisplaySurfaceCreate:
                await this.msgDisplaySurfaceCreate(h)
                break

            case MsgDisplay.MsgDisplayDrawCopy:
                await this.msgDisplayDrawCopy(h)
                break

            case MsgDisplay.MsgDisplayMark:
                await this.msgDisplayMark(h)
                break

            default:
                throw ("Unsupported message type: " + MsgDisplay[h.type])
        }
    }

    async onReady() {
        this.adapter = await navigator.gpu!.requestAdapter()
        this.device = await this.adapter!.requestDevice()

        // TODO: THIS SHOULD BE CONFIGURABLE & STUFF
        const msg = new MsgcDisplayInit(1, BigInt(this.cacheSize), 0, 0)
        this.sendMsg(MsgcDisplay.MsgcDisplayInit, msg.marshal())
    }

    async msgDisplayInvalAllPalettes(h: SpiceMiniDataHeader) {
        const buf = await this.buf.read(h.size)
        const msg = MsgDisplayInvalAllPalettes.unmarshal(buf)
        console.debug(">>> ", msg)

        // TODO: Remove cache
    }

    async msgDisplaySurfaceCreate(h: SpiceMiniDataHeader) {
        const buf = await this.buf.read(h.size)
        const msg = MsgDisplaySurfaceCreate.unmarshal(buf)
        console.debug(">>> ", msg)

        const canvas = this.canvas[msg.surfaceId]
        canvas.height = msg.height
        canvas.width = msg.width

        this.context[msg.surfaceId] = canvas.getContext("webgpu")!
        this.context[msg.surfaceId].configure({
            device: this.device!,
            format: this.presentationFormat,
        })

        // this.webgl[msg.surfaceId] = new WebGL(this.context[msg.surfaceId])
    }

    async msgDisplayMark(h: SpiceMiniDataHeader) {
        // this.webgl.forEach(wgl => wgl.draw)
    }

    async msgDisplayDrawCopy(h: SpiceMiniDataHeader) {
        const buf = await this.buf.read(h.size)
        const msg = MsgDisplayDrawCopy.unmarshal(buf)
        console.debug(">>> ", msg)

        const ctx = this.context[msg.base.surfaceId]

        const src = msg.data.srcBitmap!
        console.warn(">>> ", ImageType[src.descriptor.type])

        switch (src.descriptor.type) {
            case ImageType.ImageTypeBitmap: {

                break
            }

            case ImageType.ImageTypeQuic: {
                const quic = new SpiceQuic()
                quic.from_dv(src.quic!.data!, 0)
                console.debug(">>> ", quic)

                const img = convert_spice_quic_to_web(ctx, quic)
                ctx.putImageData(img, msg.base.box.left, msg.base.box.top)

                break
            }

            case ImageType.ImageTypeLzRgb: {
                const lz = Lz.unmarshal(src.lzRgb!.data!)
                console.debug(">>> ", lz)

                const img = convert_spice_lz_to_web(ctx, lz)
                ctx.putImageData(img, msg.base.box.left, msg.base.box.top)

                break
            }

            case ImageType.ImageTypeJpeg: {
                console.debug(">>> jpeg")
                const img = await createImageBitmap(new Blob([new Uint8Array(src.jpeg!.data!)], { type: "image/jpeg" }), 0, 0, src.descriptor.width, src.descriptor.height)
                ctx.drawImage(img, msg.base.box.left, msg.base.box.top)

                break
            }

            case ImageType.ImageTypeJpegAlpha: {
                console.debug(">>> jpeg alpha")
                const img = await createImageBitmap(new Blob([new Uint8Array(src.jpegAlpha!.data!)], { type: "image/jpeg" }), 0, 0, src.descriptor.width, src.descriptor.height)
                ctx.drawImage(img, msg.base.box.left, msg.base.box.top)

                break
            }

            default:
                throw ("Unsupported image type: " + ImageType[src.descriptor.type])
        }
    }
}