import { MsgMain, MsgMainAgentToken, MsgcMain, MsgcMainAgentStart, MsgcMainAgentToken, SpiceMiniDataHeader, VDAgentAnnounceCapabilities, VDAgentMessage, VDAgentMonConfig, VDAgentMonitorsConfig, VdAgentCap, VdAgentConfigMonitorsFlag, VdAgentProtocol, VdAgentType } from "../../gen/spice";
import { Channel } from "./channel";

export class Agent {
	channel: Channel
	serverTokens = 10

	constructor(channel: Channel) {
		this.channel = channel

		const start = new MsgcMainAgentStart(this.serverTokens)
		this.channel.sendMsg(MsgcMain.MsgcMainAgentStart, start.marshal())

		// Add monitor configuration as agent capability
		const caps = (1 << VdAgentCap.VdAgentCapMonitorsConfig) | (1 << VdAgentCap.VdAgentCapReply)

		const announce = new VDAgentAnnounceCapabilities(
			1,
			caps,
		)

		this.sendAgentMsg(VdAgentType.VdAgentTypeAnnounceCapabilities, VDAgentAnnounceCapabilities.messageSize, announce.marshal())

		console.debug('Agent connected')
	}

	sendAgentMsg(msgType: VdAgentType, msgSize: number, buf: ArrayBuffer) {
		const msg = new VDAgentMessage(
			VdAgentProtocol,
			msgType,
			BigInt(0),
			msgSize,
			[... new Uint8Array(buf)]
		)

		this.channel.sendMsg(MsgcMain.MsgcMainAgentData, msg.marshal())
	}

	async processMsg(h: SpiceMiniDataHeader) {
		switch (h.type) {
			case MsgMain.MsgMainAgentData:
				await this.msgAgentData(h)
				break

			case MsgMain.MsgMainAgentToken:
				await this.msgAgentToken(h)
				break

			default:
				console.error("Unsupported agent message type: " + MsgMain[h.type])
				this.channel.buf.read(h.size);
		}
	}

	resize(height: number, width: number) {
		const monitorsConfig = new VDAgentMonitorsConfig(
			1,
			VdAgentConfigMonitorsFlag.Empty,
			[new VDAgentMonConfig(height, width, 32, 0, 0)],
		)

		this.sendAgentMsg(VdAgentType.VdAgentTypeMonitorsConfig, monitorsConfig.messageSize, monitorsConfig.marshal())

		console.debug('Resized to ', width, 'x', height)
	}

	async msgAgentData(h: SpiceMiniDataHeader) {
		const buf = await this.channel.buf.read(h.size)
		const msg = VDAgentMessage.unmarshal(buf)

		this.serverTokens--

		// If we've run out of tokens, request new ones!
		if (this.serverTokens === 0) {
			const tokens = new MsgcMainAgentToken(10);
			this.channel.sendMsg(MsgcMain.MsgcMainAgentToken, tokens.marshal())
		}

		console.warn('AGENT DATA!!')
		console.warn(VdAgentType[msg.type])
	}

	async msgAgentToken(h: SpiceMiniDataHeader) {
		const buf = await this.channel.buf.read(h.size)
		const msg = MsgMainAgentToken.unmarshal(buf)

		this.serverTokens += msg.numTokens
	}
}
