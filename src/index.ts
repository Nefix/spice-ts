// import { AudioFmt, SpiceChannel } from "../gen/spice"
import { SpiceChannel } from "../gen/spice"
// import { classToObject } from "./helpers"
// import { Msg, NewChannel, PlaybackData, PlaybackStart, WorkerMsg } from "./workerMessages"
import { Msg, NewChannel, WorkerMsg } from "./workerMessages"

export class Client {
    url: string
    pwd: string

    canvasId: string = "canvas"

    workers: Worker[] = []

    playbackContext?: AudioContext
    playbackWorkletNode?: AudioWorkletNode
    playbackChannels?: number
    playbackFrequency?: number

    onAgentConnected?: (client: Client) => void

    constructor(url: string, pwd: string, canvasId?: string, onAgentConnected?: (client: Client) => void) {
        this.url = url
        this.pwd = pwd

        if (canvasId) {
            this.canvasId = canvasId
        }

        this.onAgentConnected = onAgentConnected
    }

    run() {
        this.newChannel(SpiceChannel.SpiceChannelMain)
    }

    newChannel(type: SpiceChannel, connId?: number, chanId?: number) {
        const worker = new Worker(new URL("worker.ts", import.meta.url).href, {
            type: "module"
        });
        this.workers.push(worker)

        worker.postMessage({
            type: WorkerMsg.NewChannel,
            data: {
                type: type,
                url: this.url,
                pwd: this.pwd,
                connId: connId,
                chanId: chanId,
            },
        })

        worker.onmessage = async (event: MessageEvent) => {
            const msg = event.data as Msg
            switch (msg.type) {
                case WorkerMsg.NewChannel: {
                    const newChanMsg = msg.data as NewChannel
                    this.newChannel(newChanMsg.type, newChanMsg.connId, newChanMsg.chanId)
                    break
                }

                case WorkerMsg.MainAgentConnected: {
                    if (this.onAgentConnected) {
                        this.onAgentConnected(this)
                    }
                    break
                }

                // case WorkerMsg.PlaybackStart: {
                //     const start = msg.data as PlaybackStart

                //     switch (start.msg.format) {

                //         case AudioFmt.AudioFmtS16:
                //             this.playbackChannels = start.msg.channels
                //             this.playbackFrequency = start.msg.frequency

                //             this.playbackContext = new AudioContext()
                //             await this.playbackContext.audioWorklet.addModule(new URL("./audio_processors/pcm16.ts", import.meta.url))

                //             this.playbackWorkletNode = new AudioWorkletNode(this.playbackContext, "pcm16-processor")
                //             this.playbackWorkletNode.connect(this.playbackContext.destination)

                //             break

                //         default:
                //             throw ("Audio format not supported: " + start.msg.format)
                //     }

                //     break
                // }

                // case WorkerMsg.PlaybackData: {
                //     const data = msg.data as PlaybackData

                //     const audioBuf = new AudioBuffer({
                //         numberOfChannels: 1,
                //         sampleRate: this.playbackFrequency!,
                //         length: data.buf.byteLength / 2,
                //     })
                //     audioBuf.copyToChannel(new Float32Array(data.buf), 0)

                //     const source = this.playbackContext!.createBufferSource()
                //     source.buffer = audioBuf
                //     source.connect(this.playbackWorkletNode!)
                //     source.start()

                //     break
                // }

                default:
                    throw ("Unsupported worker message: " + WorkerMsg[msg.type])
            }
        }
        //     case SpiceChannel.SpiceChannelDisplay:
        //         const canvas = document.getElementById(this.canvasId)! as HTMLCanvasElement
        //         const offscreen = canvas.transferControlToOffscreen()
        //         worker.postMessage({
        //             type: WorkerMsg.DisplayCanvas,
        //             data: {
        //                 canvas: offscreen
        //             }
        //         }, [offscreen])
        //         break

        //     case SpiceChannel.SpiceChannelInputs:
        //         // document.getElementById('textArea')!.addEventListener("keydown", (event) => {
        //         //     worker.postMessage({
        //         //         type: WorkerMsg.InputsKeyDown,
        //         //         data: {
        //         //             code: classToObject(event)
        //         //         }
        //         //     })
        //         // })
        //         // document.getElementById('textArea')!.addEventListener("keyup", (event) => {
        //         //     worker.postMessage({
        //         //         type: WorkerMsg.InputsKeyUp,
        //         //         data: {
        //         //             code: classToObject(event)
        //         //         }
        //         //     })
        //         // })
        //         window.addEventListener("mousemove", (event) => {
        //             worker.postMessage({
        //                 type: WorkerMsg.InputsMouseMove,
        //                 data: {
        //                     event: classToObject(event)
        //                 }
        //             })

        //             event.preventDefault()
        //         })
        //         window.addEventListener("mousedown", (event) => {
        //             worker.postMessage({
        //                 type: WorkerMsg.InputsMouseDown,
        //                 data: {
        //                     event: classToObject(event)
        //                 }
        //             })

        //             event.preventDefault()
        //         })
        //         window.addEventListener("mouseup", (event) => {
        //             worker.postMessage({
        //                 type: WorkerMsg.InputsMouseUp,
        //                 data: {
        //                     event: classToObject(event)
        //                 }
        //             })

        //             event.preventDefault()
        //         })
        //         break
        // }
    }

    resize(height: number, width: number) {
        if (this.workers.length >= 1) {
            // Channel 0 is always going to be main
            this.workers[0].postMessage({
                type: WorkerMsg.MainResize,
                data: {
                    height,
                    width,
                }
            })
        } else {
            console.error('Attempted to resize, but main channel is not open')
        }
    }
}