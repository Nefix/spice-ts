import { SpiceChannel } from "../gen/spice"
import { Channel } from "./channel/channel"
// import { CursorChannel } from "./channel/cursor"
// import { DisplayChannel } from "./channel/display"
// import { InputsChannel } from "./channel/inputs"
import { MainChannel } from "./channel/main"
// import { PlaybackChannel } from "./channel/playback"
// import { classToObject } from "./helpers"
import { Msg, WorkerMsg, NewChannel, MainResize } from "./workerMessages"
// import { Msg, WorkerMsg, NewChannel, InputsKeyDown, InputsKeyUp, InputsMouseMove, InputsMouseDown, InputsMouseUp, DisplayCanvas } from "./workerMessages"

declare var self: Worker;
let channel: Channel

self.onmessage = (event: MessageEvent) => {
    const msg = event.data as Msg
    switch (msg.type) {
        case WorkerMsg.NewChannel:
            const newChanMsg = msg.data as NewChannel
            switch (newChanMsg.type) {
                case SpiceChannel.SpiceChannelMain:
                    channel = new MainChannel(newChanMsg.url!, newChanMsg.pwd!, newChannel, onAgentConnected, newChanMsg.connId, newChanMsg.chanId)
                    break

                // case SpiceChannel.SpiceChannelDisplay:
                //     channel = new DisplayChannel(newChanMsg.url!, newChanMsg.pwd!, newChanMsg.connId, newChanMsg.chanId)
                //     break

                // case SpiceChannel.SpiceChannelInputs:
                //     channel = new InputsChannel(newChanMsg.url!, newChanMsg.pwd!, newChanMsg.connId, newChanMsg.chanId)
                //     break

                // case SpiceChannel.SpiceChannelCursor:
                //     channel = new CursorChannel(newChanMsg.url!, newChanMsg.pwd!, newChanMsg.connId, newChanMsg.chanId)
                //     break
                // case SpiceChannel.SpiceChannelPlayback:
                //     channel = new PlaybackChannel(newChanMsg.url!, newChanMsg.pwd!, playbackStart, playbackData, newChanMsg.connId, newChanMsg.chanId)
                //     break

                default:
                    throw ("Unsupported channel type: " + SpiceChannel[newChanMsg.type])
            }
            break

        case WorkerMsg.MainResize: {
            const resize = msg.data as MainResize
            const main = channel as MainChannel
            main.resize(resize.height, resize.width)
            break
        }

        //     case WorkerMsg.DisplayCanvas: {
        //         const canvas = msg.data as DisplayCanvas
        //         const display = channel as DisplayChannel
        //         display.canvas.push(canvas.canvas)
        //         break
        //     }

        //     case WorkerMsg.InputsKeyDown: {
        //         const inputsKeyDown = msg.data as InputsKeyDown
        //         const inputs = channel as InputsChannel
        //         inputs.keyDown(inputsKeyDown.event)
        //         break
        //     }

        //     case WorkerMsg.InputsKeyUp: {
        //         const inputsKeyUp = msg.data as InputsKeyUp
        //         const inputs = channel as InputsChannel
        //         inputs.keyUp(inputsKeyUp.event)
        //         break
        //     }

        //     case WorkerMsg.InputsMouseMove: {
        //         const inputsMouseMove = msg.data as InputsMouseMove
        //         const inputs = channel as InputsChannel
        //         inputs.mouseMove(inputsMouseMove.event)
        //         break
        //     }

        //     case WorkerMsg.InputsMouseDown: {
        //         const inputsMouseDown = msg.data as InputsMouseDown
        //         const inputs = channel as InputsChannel
        //         inputs.mouseDown(inputsMouseDown.event)
        //         break
        //     }

        //     case WorkerMsg.InputsMouseUp: {
        //         const inputsMouseUp = msg.data as InputsMouseUp
        //         const inputs = channel as InputsChannel
        //         inputs.mouseUp(inputsMouseUp.event)
        //         break
        //     }

        default:
            throw ("Unsupported worker message: " + WorkerMsg[msg.type])
    }
}

const newChannel = (type: SpiceChannel, connId: number, chanId: number) => {
    self.postMessage({
        type: WorkerMsg.NewChannel,
        data: {
            type: type,
            connId: connId,
            chanId: chanId,
        }
    })
}

const onAgentConnected = () => {
    self.postMessage({
        type: WorkerMsg.MainAgentConnected,
    })
}

// const playbackStart = (msg: MsgPlaybackStart) => {
//     console.warn(msg)
//     console.warn(classToObject(msg))
//     self.postMessage({
//         type: WorkerMsg.PlaybackStart,
//         data: {
//             msg: classToObject(msg)
//         }
//     })
// }

// const playbackData = (buf: ArrayBuffer) => {
//     self.postMessage({
//         type: WorkerMsg.PlaybackData,
//         data: {
//             buf: buf,
//         }
//     })
// }
