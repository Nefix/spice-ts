export class PCM16Processor extends AudioWorkletProcessor {
    constructor() {
        super()
    }

    static processorName = "pcm16-processor"

    process(inputs: Float32Array[][], outputs: Float32Array[][]): boolean {
        console.error(outputs)

        const audio = inputs[0][0]
        console.error(inputs[0][0])
        console.error(inputs[0][1])
        const channelData = outputs[0]
        const left = channelData[0]
        const right = channelData[1]

        // TODO: This processing shouldn't be only for stereo! (params)
        let channelCounter = 0
        for (let i = 0; i < audio.length; i + 2) {
            // Left channel
            left[channelCounter] = audio[i] / 32768
            // Right channel
            right[channelCounter] = audio[i + 1] / 32768

            channelCounter++
        }

        return true
    }
}

registerProcessor(PCM16Processor.processorName, PCM16Processor)
